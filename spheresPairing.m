function [ output_args ] = spheresPairing( spheresData, objectsData )

NUMBER_OF_VIEWS = size(objectsData, 4);
COLOUR_THRESHOLD = 60;
COLOUR_GREY = 17;

% Ball objects identifier
% Dimension 1: small white
% Dimension 2: big yellow
% Dimension 3: big white
ballIDs = zeros(NUMBER_OF_VIEWS, 3);

for view = 1 : NUMBER_OF_VIEWS
    
    %imshow(uint8(objectsData(:, :, 1:3, view)));
    %hold on;
    
    colourBalls = zeros(1, 3);
    % Associate the yellow ball with second entry in the ballIDs
    for ball = 1 : 3
        ball2DCoords = floor(spheresData(view, ball, 5:6));
        ball2DCoords = reshape(ball2DCoords, 1, 2);
        %scatter(ball2DCoords(1), ball2DCoords(2), 'filled');
        %pause(2);
        colour = objectsData(ball2DCoords(2), ball2DCoords(1), 1:3, view);
        %grey = 0.21 * colour(1) + 0.72 * colour(2) + 0.07 * colour(3)
        colourBalls(ball) = colour(3);
    end
    
    minColour = 255;
    ballIndex = 0;
    for ball = 1 : 3
       if colourBalls(ball) < minColour
          minColour = colourBalls(ball);
          ballIndex = ball;
       end
    end
    ballIDs(view, 2) = ballIndex;
    
    % Associate the small white ball with the first entry in the ballIDs
    % and the big white ball with the thrid entry in the ballIDs
    area = zeros(1, 2);
    ballID = zeros(1, 2);
    i = 0;
    for ball = 1 : 3
        if ball == ballIDs(view, 2)
            continue;
        else
            i = i + 1;
            area(i) = spheresData(view, ball, 7);
            ballID(i) = ball;
        end
    end
    if area(1) < area(2)
        ballIDs(view, 1) = ballID(1);
        ballIDs(view, 3) = ballID(2);
    else
        ballIDs(view, 3) = ballID(1);
        ballIDs(view, 1) = ballID(2);
    end
    
    %imshow(uint8(objectsData(:,:, 1:3, view)));
    % IMPORTANT: Ball wise info from along 3D to along 2D (not 1D)
    %prepSpheresData = reshape(spheresData(view,:,1:4), 3, 4);
    %prepSpheresData = prepSpheresData(ballIDs(view,:)',:);
    %plotpcl(objectsData(:,:, 1:6, view), prepSpheresData);
    %pause(10);
end

output_args = ballIDs;

end


