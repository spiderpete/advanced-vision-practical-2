function [ objectsData, masks ] = extractBG( data )

global FIRST_TIME_RUN;

DIFF_THRESHOLD = 60;
MORPH_THRESHOLD = 70;
ECCENTRICITY = 0.7;

if ~FIRST_TIME_RUN
    load('var.mat');
end

FrameDim = size(data, 2);
[Xdim, Ydim, Channel] = size(data{1});

masks = zeros(Xdim, Ydim, FrameDim);
objectsData = zeros(Xdim, Ydim, Channel, FrameDim);
views = zeros(Xdim, Ydim, Channel, FrameDim);
for view = 1:size(data, 2)
    views(:,:,:,view) = data{view};
    
    % Artificial creation of a static background image for employment
    % of simple background subtraction algorithm
    if FIRST_TIME_RUN && view == 1
        region = views(401:end, 1:80, 1:3, view);
        extendBG = repmat(region, 6, 8);
        for row = 1:480
            for col = 1:640
                extendBG(row, col, 1) = region(randi(numel(region(:,:,1))));
                extendBG(row, col, 2) = region(randi(numel(region(:,:,2))));
                extendBG(row, col, 3) = region(randi(numel(region(:,:,3))));
            end
        end
        save('bg.mat', 'extendBG');
    end
    
    diff = abs(views(:,:, 1:3, view) - extendBG);
    diffMask = diff > DIFF_THRESHOLD;
    
    rigorousMask = diffMask(:,:,1) | diffMask(:,:,2) | diffMask(:,:,3);
    maskMorph = bwmorph(rigorousMask, 'open', Inf);
    maskMorph = bwareaopen(maskMorph, MORPH_THRESHOLD);
    maskMorph = bwmorph(maskMorph, 'majority');
    maskNoiseFree = bwmorph(maskMorph, 'dilate', 1);
    
    stats = regionprops(maskNoiseFree, 'Eccentricity', 'Perimeter');
    perimeter = 0;
    for region = 1 : size(stats)
        if (stats(region).Perimeter > perimeter)
            perimeter = stats(region).Perimeter;
        end
    end
    conComp = bwconncomp(maskNoiseFree);
    criteria1 = uint8(find([stats.Perimeter] ~= perimeter));
    
    criteria2 = uint8(find([stats.Eccentricity] < ECCENTRICITY));
    jointCriteria = intersect(criteria1, criteria2);
    ballyObjects = ismember(labelmatrix(conComp), jointCriteria);
    
    finalMask = imfill(ballyObjects, 'holes'); % the frame was stopping this effect from working before
    masks(:,:, view) = finalMask;
    
    for channel = 1 : 6
        objectsData(:,:, channel, view) = views(:,:, channel, view) .* finalMask;
    end
end

disp('End of background extraction');
end

