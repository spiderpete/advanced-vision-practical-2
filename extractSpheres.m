function [ spheresData ] = extractSpheres( data, masks )

% 1:3 3D coordinates of sphere centre
% 4: sphere radius
% 5:6 2D coordinates of sphere's blob
% 7: area of sphere's blob
spheresData = zeros(size(data, 4), 3, 7);

for view = 1 : size(data, 4)
    
    stats = regionprops(logical(masks(:,:, view)), 'Area', 'centroid');
    area = 0;
    for region = 1: size(stats)
       if stats(region).Area > area
           area = stats(region).Area;
       end
    end
    conComp = bwconncomp(masks(:,:, view));
    criteria = uint8(find([stats.Area] ~= area));
    
    %{
    % Mask to disply only the balls
    ballObjectsMask = ismember(labelmatrix(conComp), criteria);
    %}
    
    for ball = 1 : size(criteria, 2)
        ballData = ismember(labelmatrix(conComp), criteria(ball));
        
        ballObjectX = data(:,:, 4, view) .* ballData;
        ballObjectY = data(:,:, 5, view) .* ballData;
        ballObjectZ = data(:,:, 6, view) .* ballData;
            
        ballCoords = zeros(size(ballObjectX(ballObjectX ~= 0), 1), 3);
        
        ballCoords(:, 1) = ballObjectX(ballObjectX ~= 0);
        ballCoords(:, 2) = ballObjectY(ballObjectY ~= 0);
        ballCoords(:, 3) = ballObjectZ(ballObjectZ ~= 0);
        
        [centre, radius] = sphereFit(ballCoords);
        spheresData(view, ball,:) = [centre, radius, ...
            stats(criteria(ball)).Centroid, stats(criteria(ball)).Area];        
    end
    
    %{
    % Plot the balls' points together with the fitted spheres per view
    plotpcl(data(:,:, 1:6, view), spheresData(view,:,:));
    pause(10);
    %}
    
end

end

