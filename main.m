global FIRST_TIME_RUN;

% Identify if this is the first time the system is
% run on the current machine
FIRST_TIME_RUN = false;

% Load the necessary data
DATA = 'av_pcl.mat';
load(DATA);

% Extract away the background
[objectsData, masks] = extractBG(pcl_cell);

% Extract sphere identification information
spheresData = extractSpheres(objectsData, masks);

% Associate spheres
pairedSphereData = spheresPairing(spheresData, objectsData)

%{
for view = 1 : size(objectsData, 4)
   imshow(uint8(objectsData(:,:,1:3,view)));
   title(['View: ' num2str(view)]);
   pause(1);
end
%}

clear;