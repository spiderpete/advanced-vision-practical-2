This describes the second assignment for assessment on Advanced Vision. The main goal is to register and fuse 15 range images and then extract some structures from the fused data.

### **Task Background**

At the URL: 

http://homepages.inf.ed.ac.uk/rbf/AVDATA/AV216DATA/ 

5 files can be found: 

1. av pcl.mat (92 Mb) 
1. fscatter32.m - A support file for plotpcl.m. A modified version of the file at www.mathworks.com/matlabcentral/fileexchange/2993-fscatter3-m is used
1. plotpcl.m - displays the data for each frame 
1. sphereFit.m - fits a sphere to a set of points 
1. sphereFit test.m - demonstrates sphereFit.m 

av_pcl.mat contains the data for 15 views captured by a Kinect depth sensor. It contains a cell structure with 15 entries, for the 15 views. The i-th 480*640*6 image from this can be extracted by: I=pcl_cell{i};. Values I(:,:,1:3) are an RGB image, and values I(:,:,4:6) are registered XYZ points. plotpcl(pcl_cell{i}) will display the 3D point cloud, the RGB image and a registered depth image. 

The overall task for this assignment is to fuse the individual XYZ point clouds, extract the 9 planes, and create as much of a complete model as possible. To achieve this our plan is to write a set of programs that can: 

1. Extract and remove the ground plane from the image data 
1. Extract and describe the 3 spheres 
1. Use the 3 spheres to register and fuse the XYZ data from all images 
1. Extract the 9 planes from the data 
1. Build as complete a 3D model as possible and characterise the surfaces. 

Each of these is described in more detail below. 

### **Background plane extraction**

The textured grey background is a flat planar surface. A plane containing most of the background data points are to be extractable using one of the techniques from the lecture examples. All the 3D points from the background plane can be discarded. The plane extraction might leave some small isolated 3D points that can also be removed. 

### **Sphere extraction**

There are 3 spheres, each with different sizes (diameters yellow 40 mm, large white 40 mm and small yellowish 25 mm) and colours. After the removal of the background plane, the test object and spheres are to be the main regions that remain. The intensity image can be used to help isolate the points from each sphere, and then possibly fit a sphere to the 3D data using *sphereFit*. What is needed from this stage is the 3D position of the centres of the 3 spheres. If the sphere fit is bad, i.e. produces an unrealistic sphere, then the centre of mass of the extracted data points is to be used. 

### **Registration**

One of the views is chosen to be the baseline view that all of the views is to be registered to. E.g., the one with the largest view of the target object. Registration and fusion consists of 3 steps: 

1. Pairing the extracted spheres between the baseline and current images. 
1. Estimating the transformation E = [R | ~t] that maps the 3D sphere centres in the new image to the baseline image. The rotation R mapping the current 3 sphere centres into the orientation of the baseline image are to be estimated. The lecture algorithm with the 3 vectors formed by the difference between each pair of sphere centres is used. The translation ~t is estimated by averaging the translation vector from each of the rotated sphere centres to the baseline sphere centres. The quality of the registration can be partially evaluated by computing the normal vector of the floor and evaluating how parallel they are, similarly for the black square on top of the object to register. 
1. R and ~t are used to transform all the points from the block in the current image into the baseline view. 

### **Object plane extraction**

Once all of the 15 point sets are transformed into a common coordinate system. If the registration is too noisy to determine which points belong to each plane, the colour information associated with each data point may be used. If a point set is badly registered, then it is ignored and its deletion is reported, followed by an explanation. This gives 9 infinite planes. 

### **Complete model building**

Once the 9 infinite planes are extracted, 9 bounded 3D planar patches that correspond to the 9 faces of the cube are computed. These extracted faces are represented by 100 coloured 3D points on each face (or a set of coloured triangles). A table showing the angle in degrees between the outward facing normals of each surface is created. This is a 9x9 table.